# EmergencyX
Mit EmergencyX versuchen wir das Modding und Ökosystem der Emergency-Spielereihe zu verbessern.
Viele dieser Projekte sind Open Source, wir nutzen [GitLab](https://gitlab.com/emergencyx) und freuen uns über Feedback oder Helfer.

## Matchmaking für Emergency 4
Wir kümmern uns um das Matchmaking für Emergency 4. Unser Server vermittelt zwischen Hosts und Mitspielern.
Eine Übersicht über alle laufenden Spiele in Echtzeit ist in unserem [Serverbrowser](https://www.emergencyx.de/multiplayer/) verfügbar.

## FMS für Bieberfelde Modifikation
Ein mit der Bieberfelde (und Submodifikationen) kompatibles FMS findest du [hier](https://www.emergencyx.de/fms/).
Zum Spielen wird der dem Download beigelegte LogSyncer benötigt.
