import ServerBrowser from './components/server-browser.svelte';

const serverBrowser = new ServerBrowser({
    target: document.getElementById('serverbrowser'),
    props: {
        sessions: []
    }
});

export default serverBrowser;
