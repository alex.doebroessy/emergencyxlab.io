import FMS from './components/fms.svelte';
import './scss/fms.scss';
import './scss/fms-alternative.scss';
import './scss/fms-lahmy.scss';

const urlParams = new URLSearchParams(window.location.search);
const fms = new FMS({
    target: document.getElementById('fms'),
    props: {
        sessionId: urlParams.get('id')
    }
});

export default fms;
